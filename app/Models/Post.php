<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';
    protected $fillable = ['title', 'text', 'cat_id', 'img'];

    public function category(){
        return $this->belongsTo('App\Models\Category', 'cat_id');
    }
}
