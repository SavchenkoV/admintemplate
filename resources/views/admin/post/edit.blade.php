@extends('layouts.admin_layout')

@section('title', 'Редактировать статью')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Редактировать статью {{ $post->titile }}</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('success') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <!-- form start -->
                        <form action="{{ route('post.update', $post->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Название</label>
                                    <input type="text" class="form-control" id="name" value="{{ $post->title }}" name="title" placeholder="Введите название статьи">
                                </div>

                                <div class="form-group">
                                    <label>Текст</label>
                                    <textarea class="form-control editor" rows="3" name="text" >{!! $post->text !!}</textarea>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Категория</label>
                                    </div>
                                    <select class="custom-select" name="cat_id">
                                        <option selected>Выберите категорию...</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" @if($category->id == $post->cat_id) selected @endif>{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="feature_image">Изображение поста</label>
                                    <img src="{{ asset($post->img) }}" class="img-uploaded d-block w-25" alt="">
                                    <input readonly class="form-control" type="text" value="{{ $post->img }}" id="feature_image" hidden name="img">
                                    <a href="" class="btn btn-primary btn-sm popup_selector" data-inputid="feature_image">Выбрать картинку</a>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Обновить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
